(window.webpackJsonp = window.webpackJsonp || []).push([
  [16],
  {
    '0b8eb3e35929778b339a': function(t, e, n) {
      n.r(e);
      let r;
      const o = n('8af190b70a6bc55c6f1b');
      const i = n.n(o);
      const a = (n('7377932706387c24cd20'), n('6938d226fd372a75cbf9'));
      const c = n('c7fd554010f79f6c0ef8');
      const d = n.n(c);
      function l(t, e, n) {
        return (
          e in t
            ? Object.defineProperty(t, e, {
              value: n,
              enumerable: !0,
              configurable: !0,
              writable: !0,
            })
            : (t[e] = n),
          t
        );
      }
      let s;
      const f = Object(a.withStyles)({
        root: {
          '& label.Mui-focused': { color: 'black' },
          '& .MuiFilledInput-underline:after': { borderBottomColor: 'black' },
          '& .MuiOutlinedInput-root': {
            '& fieldset': { borderColor: 'black' },
            '&:hover fieldset': { borderColor: 'black' },
            '&.Mui-focused fieldset': { borderColor: 'yellow' },
          },
        },
      })(d.a);
      const u = function(t) {
        const e = t.objectData;
        const n = t.setObjectData;
        const o = function(t) {
          const r = t.target.value;
          const o = (function(t) {
            for (let e = 1; e < arguments.length; e++) {
              var n = arguments[e] != null ? arguments[e] : {};
              let r = Object.keys(n);
              typeof Object.getOwnPropertySymbols === 'function' &&
                (r = r.concat(
                  Object.getOwnPropertySymbols(n).filter(function(t) {
                    return Object.getOwnPropertyDescriptor(n, t).enumerable;
                  }),
                )),
              r.forEach(function(e) {
                  l(t, e, n[e]);
              });
            }
            return t;
          })({}, e);
          (o.name = r), n(o);
        };
        return (function(t, e, n, o) {
          r ||
            (r =
              (typeof Symbol === 'function' &&
                Symbol.for &&
                Symbol.for('react.element')) ||
              60103);
          const i = t && t.defaultProps;
          const a = arguments.length - 3;
          if ((e || a === 0 || (e = { children: void 0 }), e && i))
            for (const c in i) void 0 === e[c] && (e[c] = i[c]);
          else e || (e = i || {});
          if (a === 1) e.children = o;
          else if (a > 1) {
            for (var d = new Array(a), l = 0; l < a; l++)
              d[l] = arguments[l + 3];
            e.children = d;
          }
          return {
            $$typeof: r,
            type: t,
            key: void 0 === n ? null : `${n}`,
            ref: null,
            props: e,
            _owner: null,
          };
        })(f, {
          value: e.name,
          type: 'text',
          spellCheck: 'false',
          fullWidth: !0,
          onChange(t) {
            return o(t);
          },
          className: 'title-input',
        });
      };
      n('8c04547036d9a8ced110');
      let v;
      const p = function() {
        return (function(t, e, n, r) {
          s ||
            (s =
              (typeof Symbol === 'function' &&
                Symbol.for &&
                Symbol.for('react.element')) ||
              60103);
          const o = t && t.defaultProps;
          const i = arguments.length - 3;
          if ((e || i === 0 || (e = { children: void 0 }), e && o))
            for (const a in o) void 0 === e[a] && (e[a] = o[a]);
          else e || (e = o || {});
          if (i === 1) e.children = r;
          else if (i > 1) {
            for (var c = new Array(i), d = 0; d < i; d++)
              c[d] = arguments[d + 3];
            e.children = c;
          }
          return {
            $$typeof: s,
            type: t,
            key: void 0 === n ? null : `${n}`,
            ref: null,
            props: e,
            _owner: null,
          };
        })('div', {
          dangerouslySetInnerHTML: {
            __html:
              '<div id="45" class="player" style="position: relative;left: 8px;top: 10px;height: 35px;width: 24px;z-index: 91;">\n<div id="animation-idle" style="height: 35px;width: 24px;background-size: cover;background-image: url(&quot;https://hero-geek.netlify.app/dist/media/img/player-idle.gif&quot;);background-position-y: -128px;transform: scale(1);z-index: 100;">\n  <div class="cornerPoint topRight"></div>\n  <div class="cornerPoint bottomRight"></div>\n  <div class="cornerPoint bottomLeft"></div>\n  <div class="cornerPoint topLeft"></div>\n</div>\n<div id="animation-running" style="display: none;height: 35px;width: 24px;background-size: cover;transform: scale(0.9);background-image: url(&quot;https://hero-geek.netlify.app/dist/media/img/player-run.gif&quot;);z-index: 100;">\n  <div class="cornerPoint topRight"></div>\n  <div class="cornerPoint bottomRight"></div>\n  <div class="cornerPoint bottomLeft"></div>\n  <div class="cornerPoint topLeft"></div>\n</div>\n<div class="base" data-material="flesh" style="height: 11px; width: 100%; position: absolute; top: 15px;">\n  <div class="collision-detector top" data-direction="top">\n    <div class="cornerPoint topRight"></div>\n    <div class="cornerPoint bottomRight"></div>\n    <div class="cornerPoint bottomLeft"></div>\n    <div class="cornerPoint topLeft"></div>\n  </div>\n  <div class="collision-detector topRight" data-direction="topRight">\n    <div class="cornerPoint topRight"></div>\n    <div class="cornerPoint bottomRight"></div>\n    <div class="cornerPoint bottomLeft"></div>\n    <div class="cornerPoint topLeft"></div>\n  </div>\n  <div class="collision-detector right" data-direction="right">\n    <div class="cornerPoint topRight"></div>\n    <div class="cornerPoint bottomRight"></div>\n    <div class="cornerPoint bottomLeft"></div>\n    <div class="cornerPoint topLeft"></div>\n  </div>\n  <div class="collision-detector bottomRight" data-direction="bottomRight">\n    <div class="cornerPoint topRight"></div>\n    <div class="cornerPoint bottomRight"></div>\n    <div class="cornerPoint bottomLeft"></div>\n    <div class="cornerPoint topLeft"></div>\n  </div>\n  <div class="collision-detector bottom" data-direction="bottom">\n    <div class="cornerPoint topRight"></div>\n    <div class="cornerPoint bottomRight"></div>\n    <div class="cornerPoint bottomLeft"></div>\n    <div class="cornerPoint topLeft"></div>\n  </div>\n  <div class="collision-detector bottomLeft" data-direction="bottomLeft">\n    <div class="cornerPoint topRight"></div>\n    <div class="cornerPoint bottomRight"></div>\n    <div class="cornerPoint bottomLeft"></div>\n    <div class="cornerPoint topLeft"></div>\n  </div>\n  <div class="collision-detector left" data-direction="left">\n    <div class="cornerPoint topRight"></div>\n    <div class="cornerPoint bottomRight"></div>\n    <div class="cornerPoint bottomLeft"></div>\n    <div class="cornerPoint topLeft"></div>\n  </div>\n  <div class="collision-detector topLeft" data-direction="topLeft">\n    <div class="cornerPoint topRight"></div>\n    <div class="cornerPoint bottomRight"></div>\n    <div class="cornerPoint bottomLeft"></div>\n    <div class="cornerPoint topLeft"></div>\n  </div>\n  <div class="cornerPoint topRight">\n    <div class="cornerPoint topRight"></div>\n    <div class="cornerPoint bottomRight"></div>\n    <div class="cornerPoint bottomLeft"></div>\n    <div class="cornerPoint topLeft"></div>\n  </div>\n  <div class="cornerPoint bottomRight">\n    <div class="cornerPoint topRight"></div>\n    <div class="cornerPoint bottomRight"></div>\n    <div class="cornerPoint bottomLeft"></div>\n    <div class="cornerPoint topLeft"></div>\n  </div>\n  <div class="cornerPoint bottomLeft">\n    <div class="cornerPoint topRight"></div>\n    <div class="cornerPoint bottomRight"></div>\n    <div class="cornerPoint bottomLeft"></div>\n    <div class="cornerPoint topLeft"></div>\n  </div>\n  <div class="cornerPoint topLeft">\n    <div class="cornerPoint topRight"></div>\n    <div class="cornerPoint bottomRight"></div>\n    <div class="cornerPoint bottomLeft"></div>\n    <div class="cornerPoint topLeft"></div>\n  </div>\n</div>\n<div class="cornerPoint topRight">\n  <div class="cornerPoint topRight"></div>\n  <div class="cornerPoint bottomRight"></div>\n  <div class="cornerPoint bottomLeft"></div>\n  <div class="cornerPoint topLeft"></div>\n</div>\n<div class="cornerPoint bottomRight">\n  <div class="cornerPoint topRight"></div>\n  <div class="cornerPoint bottomRight"></div>\n  <div class="cornerPoint bottomLeft"></div>\n  <div class="cornerPoint topLeft"></div>\n</div>\n<div class="cornerPoint bottomLeft">\n  <div class="cornerPoint topRight"></div>\n  <div class="cornerPoint bottomRight"></div>\n  <div class="cornerPoint bottomLeft"></div>\n  <div class="cornerPoint topLeft"></div>\n</div>\n<div class="cornerPoint topLeft">\n  <div class="cornerPoint topRight"></div>\n  <div class="cornerPoint bottomRight"></div>\n  <div class="cornerPoint bottomLeft"></div>\n  <div class="cornerPoint topLeft"></div>\n</div>\n</div>',
          },
        });
      };
      n('6d073f733af0d8fb32d9');
      function b(t, e, n, r) {
        v ||
          (v =
            (typeof Symbol === 'function' &&
              Symbol.for &&
              Symbol.for('react.element')) ||
            60103);
        const o = t && t.defaultProps;
        const i = arguments.length - 3;
        if ((e || i === 0 || (e = { children: void 0 }), e && o))
          for (const a in o) void 0 === e[a] && (e[a] = o[a]);
        else e || (e = o || {});
        if (i === 1) e.children = r;
        else if (i > 1) {
          for (var c = new Array(i), d = 0; d < i; d++) c[d] = arguments[d + 3];
          e.children = c;
        }
        return {
          $$typeof: v,
          type: t,
          key: void 0 === n ? null : `${n}`,
          ref: null,
          props: e,
          _owner: null,
        };
      }
      function h(t) {
        for (let e = 1; e < arguments.length; e++) {
          var n = arguments[e] != null ? arguments[e] : {};
          let r = Object.keys(n);
          typeof Object.getOwnPropertySymbols === 'function' &&
            (r = r.concat(
              Object.getOwnPropertySymbols(n).filter(function(t) {
                return Object.getOwnPropertyDescriptor(n, t).enumerable;
              }),
            )),
          r.forEach(function(e) {
            m(t, e, n[e]);
          });
        }
        return t;
      }
      function m(t, e, n) {
        return (
          e in t
            ? Object.defineProperty(t, e, {
              value: n,
              enumerable: !0,
              configurable: !0,
              writable: !0,
            })
            : (t[e] = n),
          t
        );
      }
      function g(t, e) {
        return (
          (function(t) {
            if (Array.isArray(t)) return t;
          })(t) ||
          (function(t, e) {
            const n = [];
            let r = !0;
            let o = !1;
            let i = void 0;
            try {
              for (
                var a, c = t[Symbol.iterator]();
                !(r = (a = c.next()).done) &&
                (n.push(a.value), !e || n.length !== e);
                r = !0
              );
            } catch (t) {
              (o = !0), (i = t);
            } finally {
              try {
                r || c.return == null || c.return();
              } finally {
                if (o) throw i;
              }
            }
            return n;
          })(t, e) ||
          (function() {
            throw new TypeError(
              'Invalid attempt to destructure non-iterable instance',
            );
          })()
        );
      }
      function y(t) {
        const e = t.objectData;
        const n = t.setObjectData;
        const r = g(i.a.useState(0), 2);
        const o = r[0];
        const a = r[1];
        const c = h({ backgroundRepeat: 'repeat' }, e.under);
        return b(
          'div',
          { className: 'under-wrapper' },
          void 0,
          b('div', {
            className: 'under under-selected',
            style: c,
            onDrag(t) {
              return (function(t) {
                if ((t.preventDefault(), t.clientX !== 0)) {
                  const r = h({}, e);
                  (r.under.backgroundPosition = ''
                    .concat(t.clientX, 'px ')
                    .concat(t.clientY, 'px')),
                    n(r);
                }
              })(t);
            },
          }),
          b('div', {
            className: 'under-handler under-handler-bottom',
            style: { width: e.under.width },
            onDrag(t) {
              return (function(t) {
                if ((t.preventDefault(), t.clientY !== 0)) {
                  const r = h({}, e);
                  const i = Number(r.under.height.split('px')[0]);
                  const c = o < t.clientY ? i + 1 : i - 1;
                  (r.under.height = ''.concat(c, 'px')), n(r), a(t.clientY);
                }
              })(t);
            },
          }),
          b('div', {
            className: 'under-handler under-handler-right',
            style: { height: e.under.height },
            onDrag(t) {
              return (function(t) {
                if ((t.preventDefault(), t.clientX !== 0)) {
                  const r = h({}, e);
                  const i = Number(r.under.width.split('px')[0]);
                  const c = o < t.clientX ? i + 1 : i - 1;
                  (r.under.width = ''.concat(c, 'px')), n(r), a(t.clientX);
                }
              })(t);
            },
          }),
        );
      }
      let P;
      n('498fe33d56283c695bb5');
      function x(t, e, n, r) {
        P ||
          (P =
            (typeof Symbol === 'function' &&
              Symbol.for &&
              Symbol.for('react.element')) ||
            60103);
        const o = t && t.defaultProps;
        const i = arguments.length - 3;
        if ((e || i === 0 || (e = { children: void 0 }), e && o))
          for (const a in o) void 0 === e[a] && (e[a] = o[a]);
        else e || (e = o || {});
        if (i === 1) e.children = r;
        else if (i > 1) {
          for (var c = new Array(i), d = 0; d < i; d++) c[d] = arguments[d + 3];
          e.children = c;
        }
        return {
          $$typeof: P,
          type: t,
          key: void 0 === n ? null : `${n}`,
          ref: null,
          props: e,
          _owner: null,
        };
      }
      let w;
      const k = x(p, {});
      const S = function(t) {
        return x(
          'div',
          { className: 'canvas-wrapper' },
          void 0,
          k,
          x(y, {
            objectData: t.objectData,
            setObjectData(e) {
              return t.setObjectData(e);
            },
          }),
        );
      };
      const O = (n('b2861384ab101ba0b109'), n('572290fc0c3d9f9c7cde'));
      const j = n.n(O);
      const R = n('336be1f03a45da13ce56');
      const L = n.n(R);
      const C = n('e777244f8e08c53fe98b');
      const D = n.n(C);
      const A = n('9c830e9234ad5c36a7e4');
      const _ = n.n(A);
      const I = n('432aae369667202efa42');
      const M = n.n(I);
      const N = n('c87810b6e820b5433784');
      const z = n.n(N);
      const $ = n('8b1e1b50ffccf01b7aae');
      const B = n.n($);
      const E = n('bb815d180381847b276f');
      const T = n.n(E);
      const V = n('8eef12c383e3c845d72d');
      const X = n.n(V);
      const Y = n('80e80f602055becd595c');
      const q = n.n(Y);
      function J(t, e, n, r) {
        w ||
          (w =
            (typeof Symbol === 'function' &&
              Symbol.for &&
              Symbol.for('react.element')) ||
            60103);
        const o = t && t.defaultProps;
        const i = arguments.length - 3;
        if ((e || i === 0 || (e = { children: void 0 }), e && o))
          for (const a in o) void 0 === e[a] && (e[a] = o[a]);
        else e || (e = o || {});
        if (i === 1) e.children = r;
        else if (i > 1) {
          for (var c = new Array(i), d = 0; d < i; d++) c[d] = arguments[d + 3];
          e.children = c;
        }
        return {
          $$typeof: w,
          type: t,
          key: void 0 === n ? null : `${n}`,
          ref: null,
          props: e,
          _owner: null,
        };
      }
      function U(t) {
        for (let e = 1; e < arguments.length; e++) {
          var n = arguments[e] != null ? arguments[e] : {};
          let r = Object.keys(n);
          typeof Object.getOwnPropertySymbols === 'function' &&
            (r = r.concat(
              Object.getOwnPropertySymbols(n).filter(function(t) {
                return Object.getOwnPropertyDescriptor(n, t).enumerable;
              }),
            )),
          r.forEach(function(e) {
            F(t, e, n[e]);
          });
        }
        return t;
      }
      function F(t, e, n) {
        return (
          e in t
            ? Object.defineProperty(t, e, {
              value: n,
              enumerable: !0,
              configurable: !0,
              writable: !0,
            })
            : (t[e] = n),
          t
        );
      }
      function H(t, e) {
        return (
          (function(t) {
            if (Array.isArray(t)) return t;
          })(t) ||
          (function(t, e) {
            const n = [];
            let r = !0;
            let o = !1;
            let i = void 0;
            try {
              for (
                var a, c = t[Symbol.iterator]();
                !(r = (a = c.next()).done) &&
                (n.push(a.value), !e || n.length !== e);
                r = !0
              );
            } catch (t) {
              (o = !0), (i = t);
            } finally {
              try {
                r || c.return == null || c.return();
              } finally {
                if (o) throw i;
              }
            }
            return n;
          })(t, e) ||
          (function() {
            throw new TypeError(
              'Invalid attempt to destructure non-iterable instance',
            );
          })()
        );
      }
      let W;
      const K = Object(a.makeStyles)(function() {
        return { nested: { paddingLeft: '60px' } };
      });
      const Z = Object(a.withStyles)({
        root: {
          '& label.Mui-focused': { color: 'black' },
          '& .MuiFilledInput-underline:after': { borderBottomColor: 'black' },
          '& .MuiOutlinedInput-root': {
            '& fieldset': { borderColor: 'black' },
            '&:hover fieldset': { borderColor: 'black' },
            '&.Mui-focused fieldset': { borderColor: 'yellow' },
          },
        },
      })(d.a);
      const G = J(
        j.a,
        { component: 'div', id: 'nested-list-subheader' },
        void 0,
        'Layers:',
      );
      const Q = J(_.a, {}, void 0, J(B.a, {}));
      const tt = J(M.a, { primary: 'Under' });
      const et = J(X.a, {});
      const nt = J(q.a, {});
      const rt = J(
        D.a,
        { button: !0 },
        void 0,
        J(_.a, {}, void 0, J(T.a, {})),
        J(M.a, { primary: 'Collision Box 1' }),
      );
      const ot = J(
        D.a,
        { button: !0 },
        void 0,
        J(_.a, {}, void 0, J(T.a, {})),
        J(M.a, { primary: 'Collision Box 2' }),
      );
      const it = function(t) {
        let e;
        const n = t.objectData;
        const r = t.setObjectData;
        const o = K();
        const a = H(i.a.useState(!0), 2);
        const c = a[0];
        const d = a[1];
        return J(
          L.a,
          {
            component: 'nav',
            'aria-labelledby': 'nested-list-subheader',
            subheader: G,
            className: 'layers-wrapper',
          },
          void 0,
          J(
            D.a,
            {
              button: !0,
              onClick() {
                d(!c);
              },
            },
            void 0,
            Q,
            tt,
            c ? et : nt,
          ),
          J(
            z.a,
            { in: c, timeout: 'auto', unmountOnExit: !0 },
            void 0,
            J(
              L.a,
              { component: 'div', disablePadding: !0 },
              void 0,
              J(
                D.a,
                { className: o.nested },
                void 0,
                J(Z, {
                  label: 'Background image',
                  value: ((e = n.under.backgroundImage),
                  e.match(/\((.*?)\)/)[1].replace(/('|")/g, '')),
                  type: 'text',
                  variant: 'filled',
                  spellCheck: 'false',
                  onChange(t) {
                    return (function(t) {
                      const e = t.target.value;
                      const o = U({}, n);
                      (o.under.backgroundImage = 'url('.concat(e, ')')), r(o);
                    })(t);
                  },
                }),
              ),
              J(
                D.a,
                { className: o.nested },
                void 0,
                J(Z, {
                  label: 'Width',
                  value: n.under.width.split('px')[0],
                  type: 'number',
                  variant: 'filled',
                  onChange(t) {
                    return (function(t) {
                      const e = t.target.value;
                      const o = U({}, n);
                      (o.under.width = ''.concat(e, 'px')), r(o);
                    })(t);
                  },
                }),
              ),
              J(
                D.a,
                { className: o.nested },
                void 0,
                J(Z, {
                  label: 'Height',
                  value: n.under.height.split('px')[0],
                  type: 'number',
                  variant: 'filled',
                  onChange(t) {
                    return (function(t) {
                      const e = t.target.value;
                      const o = U({}, n);
                      (o.under.height = ''.concat(e, 'px')), r(o);
                    })(t);
                  },
                }),
              ),
              J(
                D.a,
                { className: o.nested },
                void 0,
                J(Z, {
                  label: 'Background position X',
                  value: n.under.backgroundPosition.split('px ')[0],
                  type: 'number',
                  variant: 'filled',
                  onChange(t) {
                    return (function(t) {
                      const e = t.target.value;
                      const o = U({}, n);
                      const i = o.under.backgroundPosition
                        .split('px ')[1]
                        .split('px')[0];
                      (o.under.backgroundPosition = ''
                        .concat(e, 'px ')
                        .concat(i, 'px')),
                      r(o);
                    })(t);
                  },
                }),
              ),
              J(
                D.a,
                { className: o.nested },
                void 0,
                J(Z, {
                  label: 'Background position Y',
                  value: n.under.backgroundPosition
                    .split('px ')[1]
                    .split('px')[0],
                  type: 'number',
                  variant: 'filled',
                  onChange(t) {
                    return (function(t) {
                      const e = t.target.value;
                      const o = U({}, n);
                      const i = o.under.backgroundPosition.split('px ')[0];
                      (o.under.backgroundPosition = ''
                        .concat(i, 'px ')
                        .concat(e, 'px')),
                      r(o);
                    })(t);
                  },
                }),
              ),
              J(
                D.a,
                { className: o.nested },
                void 0,
                J(Z, {
                  label: 'Background size',
                  value: n.under.backgroundSize.split('px')[0],
                  type: 'number',
                  variant: 'filled',
                  onChange(t) {
                    return (function(t) {
                      const e = t.target.value;
                      const o = U({}, n);
                      (o.under.backgroundSize = ''.concat(e, 'px')), r(o);
                    })(t);
                  },
                }),
              ),
            ),
          ),
          rt,
          ot,
        );
      };
      const at = n('2aea235afd5c55b8b19b');
      const ct = n.n(at);
      n('764aa7c0240b753a0544');
      function dt(t, e, n, r) {
        W ||
          (W =
            (typeof Symbol === 'function' &&
              Symbol.for &&
              Symbol.for('react.element')) ||
            60103);
        const o = t && t.defaultProps;
        const i = arguments.length - 3;
        if ((e || i === 0 || (e = { children: void 0 }), e && o))
          for (const a in o) void 0 === e[a] && (e[a] = o[a]);
        else e || (e = o || {});
        if (i === 1) e.children = r;
        else if (i > 1) {
          for (var c = new Array(i), d = 0; d < i; d++) c[d] = arguments[d + 3];
          e.children = c;
        }
        return {
          $$typeof: W,
          type: t,
          key: void 0 === n ? null : `${n}`,
          ref: null,
          props: e,
          _owner: null,
        };
      }
      let lt;
      const st = dt(
        'div',
        { className: 'buttons-wrapper' },
        void 0,
        dt(ct.a, { variant: 'contained' }, void 0, 'Copy JSON'),
      );
      const ft = function() {
        return st;
      };
      const ut = {
        name: 'Isometric Object Example',
        under: {
          width: '42px',
          height: '42px',
          backgroundImage:
            'url(https://us.123rf.com/450wm/microone/microone1712/microone171200005/90998588-urban-cars-seamless-texture-vector-background-isometric-cars.jpg?ver=6)',
          backgroundPosition: '23px 13px',
          backgroundSize: '450px',
        },
        sides: [
          { top: 6, left: 25, width: 10, height: 14, skew: 33, number: 1 },
          { left: 15, top: 17, width: 20, height: 14, skew: -33, number: 2 },
          { left: 4, top: 21, width: 10, height: 14, skew: 33, number: 3 },
          { left: 4, top: 9, width: 20, height: 14, skew: -33, number: 4 },
        ],
        material: 'wood',
        rotated: !1,
      };
      n('e081bc46499358fd0f25');
      function vt(t, e, n, r) {
        lt ||
          (lt =
            (typeof Symbol === 'function' &&
              Symbol.for &&
              Symbol.for('react.element')) ||
            60103);
        const o = t && t.defaultProps;
        const i = arguments.length - 3;
        if ((e || i === 0 || (e = { children: void 0 }), e && o))
          for (const a in o) void 0 === e[a] && (e[a] = o[a]);
        else e || (e = o || {});
        if (i === 1) e.children = r;
        else if (i > 1) {
          for (var c = new Array(i), d = 0; d < i; d++) c[d] = arguments[d + 3];
          e.children = c;
        }
        return {
          $$typeof: lt,
          type: t,
          key: void 0 === n ? null : `${n}`,
          ref: null,
          props: e,
          _owner: null,
        };
      }
      function pt(t, e) {
        return (
          (function(t) {
            if (Array.isArray(t)) return t;
          })(t) ||
          (function(t, e) {
            const n = [];
            let r = !0;
            let o = !1;
            let i = void 0;
            try {
              for (
                var a, c = t[Symbol.iterator]();
                !(r = (a = c.next()).done) &&
                (n.push(a.value), !e || n.length !== e);
                r = !0
              );
            } catch (t) {
              (o = !0), (i = t);
            } finally {
              try {
                r || c.return == null || c.return();
              } finally {
                if (o) throw i;
              }
            }
            return n;
          })(t, e) ||
          (function() {
            throw new TypeError(
              'Invalid attempt to destructure non-iterable instance',
            );
          })()
        );
      }
      n.d(e, 'default', function() {
        return ht;
      });
      const bt = vt(ft, {});
      function ht() {
        const t = pt(i.a.useState(ut), 2);
        const e = t[0];
        const n = t[1];
        return vt(
          'section',
          { className: 'app-container' },
          void 0,
          vt(u, {
            objectData: e,
            setObjectData(t) {
              return n(t);
            },
          }),
          vt(
            'div',
            { className: 'main-content' },
            void 0,
            vt(S, {
              objectData: e,
              setObjectData(t) {
                return n(t);
              },
            }),
            vt(it, {
              objectData: e,
              setObjectData(t) {
                return n(t);
              },
            }),
          ),
          bt,
        );
      }
    },
    '2cef23523a2370810330': function(t, e, n) {
      (t.exports = n('0e326f80368fd0b1333e')(!1)).push([t.i, '', '']);
    },
    '3aae526e5360f0e8428a': function(t, e, n) {
      Object.defineProperty(e, '__esModule', { value: !0 });
      let r;
      const o =
        (r = n('f8e648336678d73b344a')) &&
        typeof r === 'object' &&
        'default' in r
          ? r.default
          : r;
      function i(t) {
        const e = {};
        for (const n in t) {
          e[n.indexOf('--') === 0 ? n : o(n)] = t[n];
        }
        return (
          t.fallbacks &&
            (Array.isArray(t.fallbacks)
              ? (e.fallbacks = t.fallbacks.map(i))
              : (e.fallbacks = i(t.fallbacks))),
          e
        );
      }
      e.default = function() {
        return {
          onProcessStyle(t) {
            if (Array.isArray(t)) {
              for (let e = 0; e < t.length; e++) t[e] = i(t[e]);
              return t;
            }
            return i(t);
          },
          onChangeValue(t, e, n) {
            if (e.indexOf('--') === 0) return t;
            const r = o(e);
            return e === r ? t : (n.prop(r, t), null);
          },
        };
      };
    },
    '3e58f2a10d418912935c': function(t, e) {
      function n() {
        return (
          (t.exports = n =
            Object.assign ||
            function(t) {
              for (let e = 1; e < arguments.length; e++) {
                const n = arguments[e];
                for (const r in n)
                  Object.prototype.hasOwnProperty.call(n, r) && (t[r] = n[r]);
              }
              return t;
            }),
          n.apply(this, arguments)
        );
      }
      t.exports = n;
    },
    '498fe33d56283c695bb5': function(t, e, n) {
      let r = n('7ca189141ca85adb0e99');
      typeof r === 'string' && (r = [[t.i, r, '']]);
      const o = { hmr: !0, transform: void 0, insertInto: void 0 };
      n('1e4534d1d62a11482e97')(r, o);
      r.locals && (t.exports = r.locals);
    },
    '4b94f1237d18da7df7cb': function(t, e, n) {
      (t.exports = n('0e326f80368fd0b1333e')(!1)).push([
        t.i,
        '.layers-wrapper {\r\n  background-color: grey;\r\n  min-width: 300px;\r\n  min-height: 300px;\r\n  margin: 5px;\r\n}\r\n\r\n.layers-nested {\r\n  padding-left: 60px;\r\n}\r\n',
        '',
      ]);
    },
    '52e8fd33bae5ef00d5fa': function(t, e, n) {
      (t.exports = n('0e326f80368fd0b1333e')(!1)).push([
        t.i,
        '.title-input {\r\n  margin-bottom: 20px;\r\n}',
        '',
      ]);
    },
    '5f28cc9d1181184e05b5': function(t, e, n) {
      Object.defineProperty(e, '__esModule', { value: !0 }),
      (e.default = function() {
        let t = function(t, e) {
          return t.length === e.length
            ? t > e
              ? 1
              : -1
            : t.length - e.length;
        };
        return {
          onProcessStyle(e, n) {
              if (n.type !== 'style') return e;
              for (
                var r = {}, o = Object.keys(e).sort(t), i = 0;
                i < o.length;
                i++
              )
                r[o[i]] = e[o[i]];
              return r;
            },
        };
      });
    },
    '5f5f52c45755fc559f51': function(t, e, n) {
      Object.defineProperty(e, '__esModule', { value: !0 });
      const r = n('87753da1c31caf0fbb22');
      const o = r.hasCSSTOMSupport && CSS ? CSS.px : 'px';
      const i = r.hasCSSTOMSupport && CSS ? CSS.ms : 'ms';
      const a = r.hasCSSTOMSupport && CSS ? CSS.percent : '%';
      function c(t) {
        const e = /(-[a-z])/g;
        const n = function(t) {
          return t[1].toUpperCase();
        };
        const r = {};
        for (const o in t) (r[o] = t[o]), (r[o.replace(e, n)] = t[o]);
        return r;
      }
      const d = c({
        'animation-delay': i,
        'animation-duration': i,
        'background-position': o,
        'background-position-x': o,
        'background-position-y': o,
        'background-size': o,
        border: o,
        'border-bottom': o,
        'border-bottom-left-radius': o,
        'border-bottom-right-radius': o,
        'border-bottom-width': o,
        'border-left': o,
        'border-left-width': o,
        'border-radius': o,
        'border-right': o,
        'border-right-width': o,
        'border-top': o,
        'border-top-left-radius': o,
        'border-top-right-radius': o,
        'border-top-width': o,
        'border-width': o,
        margin: o,
        'margin-bottom': o,
        'margin-left': o,
        'margin-right': o,
        'margin-top': o,
        padding: o,
        'padding-bottom': o,
        'padding-left': o,
        'padding-right': o,
        'padding-top': o,
        'mask-position-x': o,
        'mask-position-y': o,
        'mask-size': o,
        height: o,
        width: o,
        'min-height': o,
        'max-height': o,
        'min-width': o,
        'max-width': o,
        bottom: o,
        left: o,
        top: o,
        right: o,
        'box-shadow': o,
        'text-shadow': o,
        'column-gap': o,
        'column-rule': o,
        'column-rule-width': o,
        'column-width': o,
        'font-size': o,
        'font-size-delta': o,
        'letter-spacing': o,
        'text-indent': o,
        'text-stroke': o,
        'text-stroke-width': o,
        'word-spacing': o,
        motion: o,
        'motion-offset': o,
        outline: o,
        'outline-offset': o,
        'outline-width': o,
        perspective: o,
        'perspective-origin-x': a,
        'perspective-origin-y': a,
        'transform-origin': a,
        'transform-origin-x': a,
        'transform-origin-y': a,
        'transform-origin-z': a,
        'transition-delay': i,
        'transition-duration': i,
        'vertical-align': o,
        'flex-basis': o,
        'shape-margin': o,
        size: o,
        grid: o,
        'grid-gap': o,
        'grid-row-gap': o,
        'grid-column-gap': o,
        'grid-template-rows': o,
        'grid-template-columns': o,
        'grid-auto-rows': o,
        'grid-auto-columns': o,
        'box-shadow-x': o,
        'box-shadow-y': o,
        'box-shadow-blur': o,
        'box-shadow-spread': o,
        'font-line-height': o,
        'text-shadow-x': o,
        'text-shadow-y': o,
        'text-shadow-blur': o,
      });
      function l(t, e, n) {
        if (!e) return e;
        if (Array.isArray(e))
          for (let r = 0; r < e.length; r++) e[r] = l(t, e[r], n);
        else if (typeof e === 'object')
          if (t === 'fallbacks') for (const o in e) e[o] = l(o, e[o], n);
          else for (const i in e) e[i] = l(`${t}-${i}`, e[i], n);
        else if (typeof e === 'number') {
          const a = n[t] || d[t];
          return a
            ? typeof a === 'function'
              ? a(e).toString()
              : `${e}${a}`
            : e.toString();
        }
        return e;
      }
      e.default = function(t) {
        void 0 === t && (t = {});
        const e = c(t);
        return {
          onProcessStyle(t, n) {
            if (n.type !== 'style') return t;
            for (const r in t) t[r] = l(r, t[r], e);
            return t;
          },
          onChangeValue(t, n) {
            return l(n, t, e);
          },
        };
      };
    },
    '632cc1e17c68d05a594d': function(t, e, n) {
      Object.defineProperty(e, '__esModule', { value: !0 });
      const r =
        typeof Symbol === 'function' && typeof Symbol.iterator === 'symbol'
          ? function(t) {
            return typeof t;
          }
          : function(t) {
            return t &&
                typeof Symbol === 'function' &&
                t.constructor === Symbol &&
                t !== Symbol.prototype
              ? 'symbol'
              : typeof t;
          };
      const o = (e.isBrowser =
        (typeof window === 'undefined' ? 'undefined' : r(window)) ===
          'object' &&
        (typeof document === 'undefined' ? 'undefined' : r(document)) ===
          'object' &&
        document.nodeType === 9);
      e.default = o;
    },
    '6d073f733af0d8fb32d9': function(t, e, n) {
      let r = n('c04d9fabb7ff7f3932d9');
      typeof r === 'string' && (r = [[t.i, r, '']]);
      const o = { hmr: !0, transform: void 0, insertInto: void 0 };
      n('1e4534d1d62a11482e97')(r, o);
      r.locals && (t.exports = r.locals);
    },
    '70628e7f3df5e49104a6': function(t, e, n) {
      Object.defineProperty(e, '__esModule', { value: !0 });
      (r = n('de2cf1827168a807d23d')) &&
        typeof r === 'object' &&
        'default' in r &&
        r.default;
      let r;
      const o = n('87753da1c31caf0fbb22');
      let i = Date.now();
      const a = `fnValues${i}`;
      const c = `fnStyle${++i}`;
      e.default = function() {
        return {
          onCreateRule(t, e, n) {
            if (typeof e !== 'function') return null;
            const r = o.createRule(t, {}, n);
            return (r[c] = e), r;
          },
          onProcessStyle(t, e) {
            if (a in e || c in e) return t;
            const n = {};
            for (const r in t) {
              const o = t[r];
              typeof o === 'function' && (delete t[r], (n[r] = o));
            }
            return (e[a] = n), t;
          },
          onUpdate(t, e, n, r) {
            const o = e;
            const i = o[c];
            i && (o.style = i(t) || {});
            const d = o[a];
            if (d) for (const l in d) o.prop(l, d[l](t), r);
          },
        };
      };
    },
    '7377932706387c24cd20': function(t, e, n) {
      let r = n('52e8fd33bae5ef00d5fa');
      typeof r === 'string' && (r = [[t.i, r, '']]);
      const o = { hmr: !0, transform: void 0, insertInto: void 0 };
      n('1e4534d1d62a11482e97')(r, o);
      r.locals && (t.exports = r.locals);
    },
    '764aa7c0240b753a0544': function(t, e, n) {
      let r = n('c7f6b7ceb9a3d0710948');
      typeof r === 'string' && (r = [[t.i, r, '']]);
      const o = { hmr: !0, transform: void 0, insertInto: void 0 };
      n('1e4534d1d62a11482e97')(r, o);
      r.locals && (t.exports = r.locals);
    },
    '7ca189141ca85adb0e99': function(t, e, n) {
      (t.exports = n('0e326f80368fd0b1333e')(!1)).push([
        t.i,
        '.canvas-wrapper {\r\n  position: relative;\r\n  min-width: 300px;\r\n  min-height: 300px;\r\n  margin: 5px;\r\n  background-color: grey;\r\n}',
        '',
      ]);
    },
    '88a148375df2d0819402': function(t, e, n) {
      Object.defineProperty(e, '__esModule', { value: !0 });
      const r = n('9aa979586e9a43fe2acc');
      const o = n('87753da1c31caf0fbb22');
      e.default = function() {
        function t(e) {
          for (const n in e) {
            const i = e[n];
            if (n === 'fallbacks' && Array.isArray(i)) e[n] = i.map(t);
            else {
              let a = !1;
              const c = r.supportedProperty(n);
              c && c !== n && (a = !0);
              let d = !1;
              const l = r.supportedValue(c, o.toCssValue(i));
              l && l !== i && (d = !0),
              (a || d) && (a && delete e[n], (e[c || n] = l || i));
            }
          }
          return e;
        }
        return {
          onProcessRule(t) {
            if (t.type === 'keyframes') {
              const e = t;
              e.at = r.supportedKeyframes(e.at);
            }
          },
          onProcessStyle(e, n) {
            return n.type !== 'style' ? e : t(e);
          },
          onChangeValue(t, e) {
            return r.supportedValue(e, o.toCssValue(t)) || t;
          },
        };
      };
    },
    '8c04547036d9a8ced110': function(t, e, n) {
      let r = n('2cef23523a2370810330');
      typeof r === 'string' && (r = [[t.i, r, '']]);
      const o = { hmr: !0, transform: void 0, insertInto: void 0 };
      n('1e4534d1d62a11482e97')(r, o);
      r.locals && (t.exports = r.locals);
    },
    '988b7bbe4d0c07f69a17': function(t, e) {
      function n(t) {
        let e;
        let r;
        let o = '';
        if (typeof t === 'string' || typeof t === 'number') o += t;
        else if (typeof t === 'object')
          if (Array.isArray(t))
            for (e = 0; e < t.length; e++)
              t[e] && (r = n(t[e])) && (o && (o += ' '), (o += r));
          else for (e in t) t[e] && (o && (o += ' '), (o += e));
        return o;
      }
      t.exports = function() {
        for (var t, e, r = 0, o = ''; r < arguments.length; )
          (t = arguments[r++]) && (e = n(t)) && (o && (o += ' '), (o += e));
        return o;
      };
    },
    b2861384ab101ba0b109(t, e, n) {
      let r = n('4b94f1237d18da7df7cb');
      typeof r === 'string' && (r = [[t.i, r, '']]);
      const o = { hmr: !0, transform: void 0, insertInto: void 0 };
      n('1e4534d1d62a11482e97')(r, o);
      r.locals && (t.exports = r.locals);
    },
    c04d9fabb7ff7f3932d9(t, e, n) {
      (t.exports = n('0e326f80368fd0b1333e')(!1)).push([
        t.i,
        '.under-wrapper {\r\n  position: absolute;\r\n  top: 50%;\r\n  left: 50%;\r\n  transform: translate(-50%, -50%);\r\n}\r\n\r\n.under {\r\n  \r\n}\r\n\r\n.under-selected {\r\n  background-color: red;\r\n  border: 1px solid;\r\n  cursor: move;\r\n}\r\n\r\n.under-handler {\r\n  position: absolute;\r\n  background: #00ff00;\r\n  opacity: .2;\r\n}\r\n\r\n.under-handler:hover {\r\n  opacity: 1;\r\n}\r\n\r\n.under-handler-bottom {\r\n  bottom: -10px;\r\n  height: 4px;\r\n  cursor: n-resize;\r\n}\r\n\r\n.under-handler-right {\r\n  right: -10px;\r\n  top: 0;\r\n  width: 4px;\r\n  cursor: e-resize;\r\n}',
        '',
      ]);
    },
    c7f6b7ceb9a3d0710948(t, e, n) {
      (t.exports = n('0e326f80368fd0b1333e')(!1)).push([
        t.i,
        '.buttons-wrapper {\r\n  display: flex;\r\n}',
        '',
      ]);
    },
    e081bc46499358fd0f25(t, e, n) {
      let r = n('ff180e8fd4df45c2816c');
      typeof r === 'string' && (r = [[t.i, r, '']]);
      const o = { hmr: !0, transform: void 0, insertInto: void 0 };
      n('1e4534d1d62a11482e97')(r, o);
      r.locals && (t.exports = r.locals);
    },
    e565e1f1917079c8c46c(t, e, n) {
      function r(t) {
        return t && typeof t === 'object' && 'default' in t ? t.default : t;
      }
      Object.defineProperty(e, '__esModule', { value: !0 });
      const o = r(n('3e58f2a10d418912935c'));
      const i = (r(n('de2cf1827168a807d23d')), /\s*,\s*/g);
      const a = /&/g;
      const c = /\$([\w-]+)/g;
      e.default = function() {
        function t(t, e) {
          return function(n, r) {
            let o = t.getRule(r) || (e && e.getRule(r));
            return o ? (o = o).selector : r;
          };
        }
        function e(t, e) {
          for (
            var n = e.split(i), r = t.split(i), o = '', c = 0;
            c < n.length;
            c++
          )
            for (let d = n[c], l = 0; l < r.length; l++) {
              const s = r[l];
              o && (o += ', '),
                (o += s.indexOf('&') !== -1 ? s.replace(a, d) : `${d} ${s}`);
            }
          return o;
        }
        function n(t, e, n) {
          if (n) return o({}, n, { index: n.index + 1 });
          let r = t.options.nestingLevel;
          r = void 0 === r ? 1 : r + 1;
          const i = o({}, t.options, {
            nestingLevel: r,
            index: e.indexOf(t) + 1,
          });
          return delete i.name, i;
        }
        return {
          onProcessStyle(r, i, a) {
            if (i.type !== 'style') return r;
            let d;
            let l;
            const s = i;
            const f = s.options.parent;
            for (const u in r) {
              const v = u.indexOf('&') !== -1;
              const p = u[0] === '@';
              if (v || p) {
                if (((d = n(s, f, d)), v)) {
                  let b = e(u, s.selector);
                  l || (l = t(f, a)),
                  (b = b.replace(c, l)),
                  f.addRule(b, r[u], o({}, d, { selector: b }));
                } else
                  p &&
                    f
                      .addRule(u, {}, d)
                      .addRule(s.key, r[u], { selector: s.selector });
                delete r[u];
              }
            }
            return r;
          },
        };
      };
    },
    f8e648336678d73b344a(t, e, n) {
      const r = /[A-Z]/g;
      const o = /^ms-/;
      const i = {};
      function a(t) {
        return `-${t.toLowerCase()}`;
      }
      t.exports = function(t) {
        if (i.hasOwnProperty(t)) return i[t];
        const e = t.replace(r, a);
        return (i[t] = o.test(e) ? `-${e}` : e);
      };
    },
    ff180e8fd4df45c2816c(t, e, n) {
      (t.exports = n('0e326f80368fd0b1333e')(!1)).push([
        t.i,
        '.app-container {\r\n  margin: 10px 20px;\r\n}\r\n\r\n.main-content {\r\n  display: flex;\r\n  justify-content: space-around;\r\n  align-items: center;\r\n  align-content: stretch;\r\n  flex-wrap: wrap;\r\n  margin-bottom: 20px;\r\n}',
        '',
      ]);
    },
  },
]);
