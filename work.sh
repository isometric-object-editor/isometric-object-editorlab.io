PUBLIC_PATH='https://isometric-object-editor.gitlab.io/app/' BUILD_FOLDER_PATH='public/app' npm run build

git add .

if [ -z "$1" ]; then
  git commit --no-verify --no-status --author="isometric-object-editor <lurckm@bk.ru>" -m "Update"
else
  git commit --no-verify --no-status --author="isometric-object-editor <lurckm@bk.ru>" -m "$1"
fi

git push
