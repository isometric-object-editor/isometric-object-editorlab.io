import React from 'react';
import { ObjectTitle } from '../../components/ObjectTitle';
import { Canvas } from '../../components/Canvas';
import { Layers } from '../../components/Layers';
import { Buttons } from '../../components/Buttons';
import { MockObject } from '../../data/MockObject';
import './styles.css';

export default function HomePage() {
  const [objectData, setObjectData] = React.useState(MockObject);
  const [isCollisionVisible, setIsCollisionVisible] = React.useState(false);

  return (
    <section className="app-container">
      <ObjectTitle
        objectData={objectData}
        setObjectData={newObjectData => setObjectData(newObjectData)}
      />
      <div className="main-content">
        <Canvas
          objectData={objectData}
          setObjectData={newObjectData => setObjectData(newObjectData)}
          isCollisionVisible={isCollisionVisible}
        />
        <Layers
          objectData={objectData}
          setObjectData={newObjectData => setObjectData(newObjectData)}
          isCollisionVisible={isCollisionVisible}
          setIsCollisionVisible={newBoolean =>
            setIsCollisionVisible(newBoolean)
          }
        />
      </div>
      <Buttons objectData={objectData} />
    </section>
  );
}
