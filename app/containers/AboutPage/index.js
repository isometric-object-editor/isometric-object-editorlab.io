import React from 'react';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

export default function AboutPage() {
  return (
    <section>
      <h1>
        <FormattedMessage {...messages.header} />
      </h1>
      <a href="/app">Home</a>
    </section>
  );
}
