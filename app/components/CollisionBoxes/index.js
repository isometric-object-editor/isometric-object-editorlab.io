/* eslint-disable react/prop-types */
import React from 'react';
import './styles.css';

export const CollisionBoxes = props => {
  const { objectData } = props;

  const SidesHolder = ({ sides }) => (
    <>
      {sides.map(side => (
        <div
          className="collision-side"
          key={side.number}
          style={{ position: 'absolute', ...objectData.sides[side.number] }}
        />
      ))}
    </>
  );

  return (
    <div
      className={`collision-boxes-wrapper ${
        props.isCollisionVisible ? '' : 'hidden'
      }`}
    >
      <SidesHolder sides={objectData.sides} />
    </div>
  );
};
