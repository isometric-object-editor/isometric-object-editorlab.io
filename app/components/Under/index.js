import React from 'react';
import './styles.css';
import { CollisionBoxes } from '../CollisionBoxes';

export const Under = props => {
  const { objectData, setObjectData } = props;

  const style = {
    backgroundRepeat: 'repeat',
    ...objectData.under,
  };

  return (
    <div className="under-wrapper">
      <div className="under" style={style}>
        <CollisionBoxes
          objectData={props.objectData}
          setObjectData={newObjectData => props.setObjectData(newObjectData)}
          isCollisionVisible={props.isCollisionVisible}
        />
      </div>
    </div>
  );
};
