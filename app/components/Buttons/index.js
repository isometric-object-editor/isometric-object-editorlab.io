/* eslint-disable react/prop-types */
import React from 'react';
import Button from '@material-ui/core/Button';
import './styles.css';

export const Buttons = props => {
  const { objectData } = props;

  const handleClick = () => {
    navigator.clipboard.writeText(JSON.stringify(objectData));
  };

  return (
    <div className="buttons-wrapper">
      <Button
        variant="contained"
        onClick={() => {
          handleClick();
        }}
      >
        Copy JSON
      </Button>
    </div>
  );
};
