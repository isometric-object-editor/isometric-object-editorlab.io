import React from 'react';
import './styles.css';
import { BoxControl } from '../BoxControl';

export const CollisionBoxesControls = props => {
  const { objectData, setObjectData } = props;

  const BoxesControlsHolder = ({ sides }) => (
    <>
      {sides.map(side => (
        <BoxControl
          key={side.number}
          lastSideNumber={side.number}
          objectData={props.objectData}
          setObjectData={newObjectData => props.setObjectData(newObjectData)}
        />
      ))}
    </>
  );

  return <BoxesControlsHolder sides={objectData.sides} />;
};
