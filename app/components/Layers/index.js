/* eslint-disable react/prop-types */
/* eslint-disable no-plusplus */
import React from 'react';
import './styles.css';

import { makeStyles, withStyles } from '@material-ui/core/styles';
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import PanoramaIcon from '@material-ui/icons/Panorama';
import AddBoxIcon from '@material-ui/icons/AddBox';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

import TextField from '@material-ui/core/TextField';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';

import { MockObstacleSides } from '../../data/MockObstacleSides';

import { CollisionBoxesControls } from '../CollisionBoxesControls';

import { getSrcFromBackgroundImageUrl } from '../../utils/getSrcFromBackgroundImageUrl';
const cloneDeep = require('lodash/fp/cloneDeep');

const useStyles = makeStyles(() => ({
  nested: {
    paddingLeft: '60px',
  },
  greenItem: {
    backgroundColor: '#4caf50aa',
    '&:hover, &:focus': {
      backgroundColor: '#4caf50',
    },
  },
}));

const CssTextField = withStyles({
  root: {
    '& label.Mui-focused': {
      color: 'black',
    },
    '& .MuiFilledInput-underline:after': {
      borderBottomColor: 'black',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: 'black',
      },
      '&:hover fieldset': {
        borderColor: 'black',
      },
      '&.Mui-focused fieldset': {
        borderColor: 'yellow',
      },
    },
  },
})(TextField);

export const Layers = props => {
  const { objectData, setObjectData } = props;

  const classes = useStyles();
  const [open, setOpen] = React.useState(true);

  const handleClick = () => {
    setOpen(!open);
  };

  const onWidthChange = e => {
    const newWidth = e.target.value;
    const newObjectData = { ...objectData };
    newObjectData.under.width = `${newWidth}px`;
    setObjectData(newObjectData);
  };

  const onHeightChange = e => {
    const newHeight = e.target.value;
    const newObjectData = { ...objectData };
    newObjectData.under.height = `${newHeight}px`;
    setObjectData(newObjectData);
  };

  const onBackgroundChange = e => {
    const newBackgroundImage = e.target.value;
    const newObjectData = { ...objectData };
    newObjectData.under.backgroundImage = `url(${newBackgroundImage})`;
    setObjectData(newObjectData);
  };

  const onBgPosXChange = e => {
    const posX = e.target.value;
    const newObjectData = { ...objectData };
    const posY = newObjectData.under.backgroundPosition
      .split('px ')[1]
      .split('px')[0];
    newObjectData.under.backgroundPosition = `${posX}px ${posY}px`;
    setObjectData(newObjectData);
  };

  const onBgPosYChange = e => {
    const posY = e.target.value;
    const newObjectData = { ...objectData };
    const posX = newObjectData.under.backgroundPosition.split('px ')[0];
    newObjectData.under.backgroundPosition = `${posX}px ${posY}px`;
    setObjectData(newObjectData);
  };

  const onBackgroundSizeChange = e => {
    const newSize = e.target.value;
    const newObjectData = { ...objectData };
    newObjectData.under.backgroundSize = `${newSize}px`;
    setObjectData(newObjectData);
  };

  const addNewCollisionBox = () => {
    const newObjectData = { ...objectData };
    const arrayOfNewSides = cloneDeep(MockObstacleSides, false);
    newObjectData.sides = [...newObjectData.sides, ...arrayOfNewSides];
    newObjectData.sides.map((side, index) => {
      const newSide = side;
      newSide.number = index;
      return newSide;
    });
    setObjectData(newObjectData);
  };

  const handleCollisionVisibilitySwitch = () => {
    props.setIsCollisionVisible(!props.isCollisionVisible);
  };

  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      // subheader={
      //   <ListSubheader component="div" id="nested-list-subheader">
      //     Layers:
      //   </ListSubheader>
      // }
      className="layers-wrapper"
    >
      <ListItem button onClick={handleClick}>
        <ListItemIcon>
          <PanoramaIcon />
        </ListItemIcon>
        <ListItemText primary="Under" />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <ListItem className={classes.nested}>
            <CssTextField
              label="Background image"
              value={getSrcFromBackgroundImageUrl(
                objectData.under.backgroundImage,
              )}
              type="text"
              variant="filled"
              spellCheck="false"
              onChange={e => onBackgroundChange(e)}
            />
          </ListItem>
          <ListItem className={classes.nested}>
            <CssTextField
              label="Width"
              value={objectData.under.width.split('px')[0]}
              type="number"
              variant="filled"
              onChange={e => onWidthChange(e)}
            />
          </ListItem>
          <ListItem className={classes.nested}>
            <CssTextField
              label="Height"
              value={objectData.under.height.split('px')[0]}
              type="number"
              variant="filled"
              onChange={e => onHeightChange(e)}
            />
          </ListItem>
          <ListItem className={classes.nested}>
            <CssTextField
              label="Background position X"
              value={objectData.under.backgroundPosition.split('px ')[0]}
              type="number"
              variant="filled"
              onChange={e => onBgPosXChange(e)}
            />
          </ListItem>
          <ListItem className={classes.nested}>
            <CssTextField
              label="Background position Y"
              value={
                objectData.under.backgroundPosition
                  .split('px ')[1]
                  .split('px')[0]
              }
              type="number"
              variant="filled"
              onChange={e => onBgPosYChange(e)}
            />
          </ListItem>
          <ListItem className={classes.nested}>
            <CssTextField
              label="Background size"
              value={objectData.under.backgroundSize.split('px')[0]}
              type="number"
              variant="filled"
              onChange={e => onBackgroundSizeChange(e)}
            />
          </ListItem>
        </List>
      </Collapse>
      <ListItem className={classes.greenItem}>
        <ListItemIcon>
          {props.isCollisionVisible ? (
            <VisibilityIcon />
          ) : (
            <VisibilityOffIcon />
          )}
        </ListItemIcon>
        <FormGroup row>
          <FormControlLabel
            control={
              <Switch
                checked={props.isCollisionVisible}
                onChange={handleCollisionVisibilitySwitch}
                name="collisionVisibilitySwitch"
                color="primary"
              />
            }
            label="See Collision Boxes"
          />
        </FormGroup>
      </ListItem>
      <CollisionBoxesControls
        objectData={props.objectData}
        setObjectData={newObjectData => props.setObjectData(newObjectData)}
      />
      <ListItem
        button
        disabled={props.objectData.sides.length >= 12}
        onClick={addNewCollisionBox}
        className={classes.greenItem}
      >
        <ListItemIcon>
          <AddBoxIcon />
        </ListItemIcon>
        <ListItemText primary="New Collision Box" />
      </ListItem>
    </List>
  );
}
