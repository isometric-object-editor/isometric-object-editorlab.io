import React from 'react';
import './styles.css';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const CssTextField = withStyles({
  root: {
    '& label.Mui-focused': {
      color: 'black',
    },
    '& .MuiFilledInput-underline:after': {
      borderBottomColor: 'black',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: 'black',
      },
      '&:hover fieldset': {
        borderColor: 'black',
      },
      '&.Mui-focused fieldset': {
        borderColor: 'yellow',
      },
    },
  },
})(TextField);

export const ObjectTitle = props => {
  const { objectData, setObjectData } = props;

  const onTitleChange = e => {
    const newTitle = e.target.value;
    const newObjectData = { ...objectData };
    newObjectData.name = newTitle;
    setObjectData(newObjectData);
  };

  return (
    <CssTextField
      value={objectData.name}
      type="text"
      spellCheck="false"
      fullWidth
      onChange={e => onTitleChange(e)}
      className="title-input"
    />
  );
};
