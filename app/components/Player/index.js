import React from 'react';
import './styles.css';

const playerMarkup = `<div id="player" class="player" style="position: relative;left: 8px;top: 10px;height: 35px;width: 24px;z-index: 91;">
<div id="animation-idle" style="height: 35px;width: 24px;background-size: cover;background-image: url(&quot;https://hero-geek.netlify.app/dist/media/img/player-idle.gif&quot;);background-position-y: -128px;transform: scale(1);z-index: 100;">
  <div class="cornerPoint topRight"></div>
  <div class="cornerPoint bottomRight"></div>
  <div class="cornerPoint bottomLeft"></div>
  <div class="cornerPoint topLeft"></div>
</div>
<div id="animation-running" style="display: none;height: 35px;width: 24px;background-size: cover;transform: scale(0.9);background-image: url(&quot;https://hero-geek.netlify.app/dist/media/img/player-run.gif&quot;);z-index: 100;">
  <div class="cornerPoint topRight"></div>
  <div class="cornerPoint bottomRight"></div>
  <div class="cornerPoint bottomLeft"></div>
  <div class="cornerPoint topLeft"></div>
</div>
<div class="base" data-material="flesh" style="height: 11px; width: 100%; position: absolute; top: 15px;">
  <div class="collision-detector top" data-direction="top">
    <div class="cornerPoint topRight"></div>
    <div class="cornerPoint bottomRight"></div>
    <div class="cornerPoint bottomLeft"></div>
    <div class="cornerPoint topLeft"></div>
  </div>
  <div class="collision-detector topRight" data-direction="topRight">
    <div class="cornerPoint topRight"></div>
    <div class="cornerPoint bottomRight"></div>
    <div class="cornerPoint bottomLeft"></div>
    <div class="cornerPoint topLeft"></div>
  </div>
  <div class="collision-detector right" data-direction="right">
    <div class="cornerPoint topRight"></div>
    <div class="cornerPoint bottomRight"></div>
    <div class="cornerPoint bottomLeft"></div>
    <div class="cornerPoint topLeft"></div>
  </div>
  <div class="collision-detector bottomRight" data-direction="bottomRight">
    <div class="cornerPoint topRight"></div>
    <div class="cornerPoint bottomRight"></div>
    <div class="cornerPoint bottomLeft"></div>
    <div class="cornerPoint topLeft"></div>
  </div>
  <div class="collision-detector bottom" data-direction="bottom">
    <div class="cornerPoint topRight"></div>
    <div class="cornerPoint bottomRight"></div>
    <div class="cornerPoint bottomLeft"></div>
    <div class="cornerPoint topLeft"></div>
  </div>
  <div class="collision-detector bottomLeft" data-direction="bottomLeft">
    <div class="cornerPoint topRight"></div>
    <div class="cornerPoint bottomRight"></div>
    <div class="cornerPoint bottomLeft"></div>
    <div class="cornerPoint topLeft"></div>
  </div>
  <div class="collision-detector left" data-direction="left">
    <div class="cornerPoint topRight"></div>
    <div class="cornerPoint bottomRight"></div>
    <div class="cornerPoint bottomLeft"></div>
    <div class="cornerPoint topLeft"></div>
  </div>
  <div class="collision-detector topLeft" data-direction="topLeft">
    <div class="cornerPoint topRight"></div>
    <div class="cornerPoint bottomRight"></div>
    <div class="cornerPoint bottomLeft"></div>
    <div class="cornerPoint topLeft"></div>
  </div>
  <div class="cornerPoint topRight">
    <div class="cornerPoint topRight"></div>
    <div class="cornerPoint bottomRight"></div>
    <div class="cornerPoint bottomLeft"></div>
    <div class="cornerPoint topLeft"></div>
  </div>
  <div class="cornerPoint bottomRight">
    <div class="cornerPoint topRight"></div>
    <div class="cornerPoint bottomRight"></div>
    <div class="cornerPoint bottomLeft"></div>
    <div class="cornerPoint topLeft"></div>
  </div>
  <div class="cornerPoint bottomLeft">
    <div class="cornerPoint topRight"></div>
    <div class="cornerPoint bottomRight"></div>
    <div class="cornerPoint bottomLeft"></div>
    <div class="cornerPoint topLeft"></div>
  </div>
  <div class="cornerPoint topLeft">
    <div class="cornerPoint topRight"></div>
    <div class="cornerPoint bottomRight"></div>
    <div class="cornerPoint bottomLeft"></div>
    <div class="cornerPoint topLeft"></div>
  </div>
</div>
<div class="cornerPoint topRight">
  <div class="cornerPoint topRight"></div>
  <div class="cornerPoint bottomRight"></div>
  <div class="cornerPoint bottomLeft"></div>
  <div class="cornerPoint topLeft"></div>
</div>
<div class="cornerPoint bottomRight">
  <div class="cornerPoint topRight"></div>
  <div class="cornerPoint bottomRight"></div>
  <div class="cornerPoint bottomLeft"></div>
  <div class="cornerPoint topLeft"></div>
</div>
<div class="cornerPoint bottomLeft">
  <div class="cornerPoint topRight"></div>
  <div class="cornerPoint bottomRight"></div>
  <div class="cornerPoint bottomLeft"></div>
  <div class="cornerPoint topLeft"></div>
</div>
<div class="cornerPoint topLeft">
  <div class="cornerPoint topRight"></div>
  <div class="cornerPoint bottomRight"></div>
  <div class="cornerPoint bottomLeft"></div>
  <div class="cornerPoint topLeft"></div>
</div>
</div>`;

export const Player = () => (
  // eslint-disable-next-line react/no-danger
  <div dangerouslySetInnerHTML={{ __html: playerMarkup }} />
);
