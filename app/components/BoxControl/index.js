/* eslint-disable react/prop-types */
/* eslint-disable no-plusplus */
import React from 'react';
import './styles.css';

import { makeStyles, withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import InputIcon from '@material-ui/icons/Input';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

import TextField from '@material-ui/core/TextField';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { getNumberFromPixels } from '../../utils/getNumberFromPixels';

const useStyles = makeStyles(() => ({
  nested: {
    paddingLeft: '60px',
  },
}));

const CssTextField = withStyles({
  root: {
    '& label.Mui-focused': {
      color: 'black',
    },
    '& .MuiFilledInput-underline:after': {
      borderBottomColor: 'black',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: 'black',
      },
      '&:hover fieldset': {
        borderColor: 'black',
      },
      '&.Mui-focused fieldset': {
        borderColor: 'yellow',
      },
    },
  },
})(TextField);

export const BoxControl = props => {
  const { objectData, setObjectData } = props;

  const classes = useStyles();
  const [open, setOpen] = React.useState(true);

  const handleClick = () => {
    setOpen(!open);
  };

  const getOurSidesNumbers = () => [props.lastSideNumber];

  const getOurSideByIndex = () => {
    const arrayOfOurSides = objectData.sides.filter(
      side => side.number === props.lastSideNumber,
    );

    return arrayOfOurSides[0];
  };

  const getAverage = direction => {
    // get 4 sides of the box and calculate average top or left
    const arrayOfOurSidesNumbers = getOurSidesNumbers();
    const arrayOfOurSides = objectData.sides.filter(side =>
      arrayOfOurSidesNumbers.includes(side.number),
    );
    const arrayOfValues = [];
    arrayOfOurSides.forEach(side =>
      arrayOfValues.push(getNumberFromPixels(side[direction])),
    );

    const average = arr => arr.reduce((a, b) => a + b) / arr.length;

    return Math.round(average(arrayOfValues));
  };

  const changeBoxPos = (e, direction) => {
    const newAverage = Number(e.target.value);
    const sidesAverage = getAverage(direction);
    const arrayOfOurSidesNumbers = getOurSidesNumbers();

    const newObjectData = { ...objectData };
    newObjectData.sides.map(side => {
      const newSide = side;
      if (arrayOfOurSidesNumbers.includes(side.number)) {
        const prevVal = getNumberFromPixels(newSide[direction]);
        newSide[direction] = `${prevVal + (newAverage - sidesAverage)}px`;
      }
      return newSide;
    });
    setObjectData(newObjectData);

    // The TextField loses focus on rerender - no time to hustle about
    setTimeout(() => {
      document
        .getElementById(
          `input-collision-box-${props.lastSideNumber}-${direction}`,
        )
        .focus();
    }, 10);
  };

  const onXChange = e => {
    changeBoxPos(e, 'left');
  };

  const onYChange = e => {
    changeBoxPos(e, 'top');
  };

  const changeBoxWidth = e => {
    const ourSides = {
      side0: getOurSideByIndex(0),
    };
    const newWidth = Number(e.target.value);
    const prevWidth = getNumberFromPixels(ourSides.side0.width);
    const wantedSides = [ourSides.side0.number, ourSides.side0.number];

    const newObjectData = { ...objectData };
    newObjectData.sides.map(currentSide => {
      const newSide = currentSide;
      if (wantedSides.includes(currentSide.number)) {
        // side 0 and side 2
        newSide.width = `${newWidth}px`;
      }
      return newSide;
    });
    setObjectData(newObjectData);

    // The TextField loses focus on rerender - no time to hustle about
    setTimeout(() => {
      document
        .getElementById(`input-collision-box-${props.lastSideNumber}-width`)
        .focus();
    }, 10);
  };

  const onWidthChange = e => {
    changeBoxWidth(e);
  };

  const changeBoxHeight = e => {
    const newHeight = Number(e.target.value);
    const wantedSides = getOurSidesNumbers();

    const newObjectData = { ...objectData };
    newObjectData.sides.map(currentSide => {
      const newSide = currentSide;
      if (wantedSides.includes(currentSide.number)) {
        newSide.height = `${newHeight}px`;
      }
      return newSide;
    });
    setObjectData(newObjectData);

    // The TextField loses focus on rerender - no time to hustle about
    setTimeout(() => {
      document
        .getElementById(`input-collision-box-${props.lastSideNumber}-height`)
        .focus();
    }, 10);
  };

  const onHeightChange = e => {
    changeBoxHeight(e);
  };

  const onRemoveBox = () => {
    const wantedSides = getOurSidesNumbers();
    const newObjectData = { ...objectData };
    const newSides = newObjectData.sides.filter(
      side => !wantedSides.includes(side.number),
    );
    newSides.map((side, index) => {
      const newSide = side;
      newSide.number = index;
      return newSide;
    });
    newObjectData.sides = newSides;
    setObjectData(newObjectData);
  };

  return (
    <List component="div" disablePadding>
      <ListItem button onClick={handleClick}>
        <ListItemIcon>
          <InputIcon />
        </ListItemIcon>
        <ListItemText primary={`Collision Box ${props.lastSideNumber}`} />
        {open ? <ExpandLess /> : <ExpandMore />}
        <DeleteForeverIcon button="true" onClick={() => onRemoveBox()} />
      </ListItem>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <ListItem className={classes.nested}>
            <CssTextField
              label="X"
              value={getAverage('left')}
              type="number"
              variant="filled"
              spellCheck="false"
              onChange={e => onXChange(e)}
              id={`input-collision-box-${props.lastSideNumber}-left`}
            />
          </ListItem>
          <ListItem className={classes.nested}>
            <CssTextField
              label="Y"
              value={getAverage('top')}
              type="number"
              variant="filled"
              spellCheck="false"
              onChange={e => onYChange(e)}
              id={`input-collision-box-${props.lastSideNumber}-top`}
            />
          </ListItem>
          <ListItem className={classes.nested}>
            <CssTextField
              label="Width"
              value={getNumberFromPixels(getOurSideByIndex(0).width)}
              type="number"
              variant="filled"
              spellCheck="false"
              onChange={e => onWidthChange(e)}
              id={`input-collision-box-${props.lastSideNumber}-width`}
            />
          </ListItem>
          <ListItem className={classes.nested}>
            <CssTextField
              label="Height"
              value={getNumberFromPixels(getOurSideByIndex(0).height)}
              type="number"
              variant="filled"
              spellCheck="false"
              onChange={e => onHeightChange(e)}
              id={`input-collision-box-${props.lastSideNumber}-height`}
            />
          </ListItem>
        </List>
      </Collapse>
    </List>
  );
};
