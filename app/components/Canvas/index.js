import React from 'react';
import { Player } from '../Player';
import { Under } from '../Under';
import './styles.css';

export const Canvas = props => (
  <div className="canvas-wrapper">
    {/* <Player /> */}
    <Under
      objectData={props.objectData}
      setObjectData={newObjectData => props.setObjectData(newObjectData)}
      isCollisionVisible={props.isCollisionVisible}
    />
  </div>
);
