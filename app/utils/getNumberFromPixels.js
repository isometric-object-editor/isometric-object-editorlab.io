export const getNumberFromPixels = str => {
  // transforms '10px' to 10
  return Number(str.split('px')[0]);
};
