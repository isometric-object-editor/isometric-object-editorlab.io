export const getSrcFromBackgroundImageUrl = str =>
  // Gets "https://example.com/image.png" from "url(https://example.com/image.png)"
  // https://stackoverflow.com/questions/20857404/regex-to-extract-url-from-css-background-styling
  str.match(/\((.*?)\)/)[1].replace(/('|")/g, '');
