import { MockObstacleSides } from './MockObstacleSides';
const cloneDeep = require('lodash/fp/cloneDeep');

export const MockObject = {
  name: 'Isometric Object Example',
  under: {
    width: '81px',
    height: '57px',
    backgroundImage: `url(https://isometric-object-editor.gitlab.io/img/cars.png)`,
    backgroundPosition: '-317px -423px',
    backgroundSize: '530px',
  },
  sides: cloneDeep(MockObstacleSides),
  material: 'wood',
  rotated: false,
};
